const fs = require("fs");
const path = require("path");
const readline = require("readline");
const axios = require("axios");
const moment = require("moment");
const qs = require("qs");

// const __dirname = path.resolve();

// 讀取 setting
const settingPath = path.resolve(__dirname, "./setting.json");
const setting = JSON.parse(fs.readFileSync(settingPath, "utf8"));

/** 日期列表 */
const dateList = [];
for (let i = 1; i <= 2; i++) {
  const date = moment().add(-i, "days").format("YYYY-MM-DD");
  dateList.push(date);
}

/** 年月列表 */
const yearMonthList = [];
for (const date of dateList) {
  const yearMonth = {
    year: date.split("-")[0],
    month: date.split("-")[1],
  };
  if (
    !yearMonthList.find(
      (item) => item.year === yearMonth.year && item.month === yearMonth.month
    )
  ) {
    yearMonthList.push(yearMonth);
  }
}

/** 行事曆 */
const calendar = [];

/** 打卡紀錄 */
let records = [];

/** 打卡地點 */
let punchesLocationId = "";

let cookie = "";
(async () => {
  try {
    // 取得 __RequestVerificationToken
    const { inputToken, cookieToken } = await getRequestVerificationToken();
    // 登入
    console.log("登入");
    cookie = await login(inputToken, cookieToken);
    // 取得班表
    console.log("取得班表");
    for (const yearMonth of yearMonthList) {
      const data = await getCalendar(yearMonth);
      calendar.push(...data);
    }
    // 取得打卡紀錄
    console.log("取得打卡紀錄");
    records = await getRecords();
    // 取得打卡地點
    console.log("取得打卡地點");
    punchesLocationId = await getLocations();
    // 打卡
    console.log("打卡");
    for (const date of dateList) {
      // 檢查有沒有上班
      const calendarItem = calendar.find((item) => item.Date.startsWith(date));
      if (!calendarItem.ShiftSchedule.WorkOnTime) {
        console.log("沒上班", date);
        continue;
      }
      if (calendarItem.LeaveSheets?.length) {
        console.log("請假", date);
        continue;
      }

      // 己查是否已打卡
      const recordsItem = records.find((item) =>
        item.AttendanceDate.startsWith(date)
      );
      if (!recordsItem?.InAttendanceOn) {
        // 打上班卡
        console.log("打上班卡", date);
        await checkIn(date, 1);
      }
      if (!recordsItem?.OutAttendanceOn) {
        // 打下班卡
        console.log("打下班卡", date);
        await checkIn(date, 2);
      }
    }

    // 登出
    console.log("登出");
    await logout();

    createReadline("完成");
  } catch (e) {
    console.log(e);
    createReadline("錯誤");
  }
})();

async function getRequestVerificationToken() {
  const url = "https://auth.mayohr.com/HRM/Account/Login";
  const res = await axios.get(url, {});

  const reg =
    /<input name="__RequestVerificationToken" type="hidden" value="(.*)" \/>/;
  const inputToken = res.data.match(reg);

  const setCookieList = res.headers["set-cookie"];
  const setCookie = setCookieList.find((item) =>
    item.includes("__RequestVerificationToken")
  );
  const cookieToken = setCookie.split(";")[0].split("=")[1];
  return { inputToken: inputToken[1], cookieToken };
}

/** 登入 */
async function login(inputToken, cookieToken) {
  const url = "https://auth.mayohr.com/Token";
  const data = {
    __RequestVerificationToken: inputToken,
    grant_type: "password",
    locale: "zh-tw",
    red: "https://apolloxe.mayohr.com/tube",
    userStatus: 1,
    UserName: setting.account,
    Password: setting.password,
  };
  const res = await axios.post(url, qs.stringify(data), {
    headers: {
      cookie: "__RequestVerificationToken=" + cookieToken,
    },
  });

  const url2 = `https://authcommon.mayohr.com/api/auth/checkticket?code=${res.data.code}`;
  const res2 = await axios.get(url2, {});
  const setCookieList = res2.headers["set-cookie"];
  const setCookie = setCookieList.find((item) =>
    item.includes("__ModuleSessionCookie")
  );
  const cookie = setCookie.split(";")[0];
  return cookie;
}
/** 取得打卡地點 */
async function getLocations() {
  const url = "https://apolloxe.mayohr.com/backend/pt/api/locations/EnableList";
  const res = await axios.get(url, {
    headers: {
      cookie: cookie,
    },
  });
  return res.data.Data[0].PunchesLocationId;
}
/** 取得班表 */
async function getCalendar(yearMonth) {
  const url = `https://apolloxe.mayohr.com/backend/pt/api/EmployeeCalendars/scheduling/V2?year=${yearMonth.year}&month=${yearMonth.month}`;
  const res = await axios.get(url, {
    headers: {
      cookie: cookie,
    },
  });
  return res.data.Data.Calendars;
}
/** 取得打卡紀錄 */
async function getRecords() {
  const url = `https://apolloxe.mayohr.com/backend/pt/api/checkinRecords/onOffWork`;
  const res = await axios.get(url, {
    params: {
      attendanceDateEnd: moment(dateList[0]).format("YYYY/MM/DD"),
      attendanceDateStart: moment(dateList[dateList.length - 1]).format(
        "YYYY/MM/DD"
      ),
      checkInWay: "workOnOff",
    },
    headers: {
      cookie: cookie,
    },
  });
  return res.data.Data;
}
/** 打卡 */
async function checkIn(date, type) {
  // type: 1 上班 2 下班
  // POST https://apolloxe.mayohr.com/backend/pt/api/reCheckInApproval
  const url = `https://apolloxe.mayohr.com/backend/pt/api/reCheckInApproval`;
  const data = {
    AttendanceOn:
      moment(date)
        .set({
          hour: type === 1 ? 9 : 18,
          minute: type === 1 ? 40 + randomInt() : 20 - randomInt(),
          second: 0,
        })
        .utc()
        .format("YYYY-MM-DDTHH:mm:ss") + "+00:00",
    AttendanceType: type,
    IsBehalf: false,
    PunchesLocationId: punchesLocationId,
    locationDetails: "location",
  };
  const res = await axios.post(url, data, {
    headers: {
      cookie: cookie,
    },
  });
  console.log(res.data.Meta);
  await sleep(5000);
}
/** 登出 */
async function logout() {
  const url = "https://auth.mayohr.com/HRM/account/logout";
  const res = await axios.get(url, {
    headers: {
      cookie: cookie,
    },
  });
}

/** 隨機0~19 */
function randomInt() {
  return Math.floor(Math.random() * 19);
}

function sleep(ms = 1000) {
  return new Promise((resolve) => setTimeout(resolve, ms));
}

function createReadline(text) {
  return readline
    .createInterface({
      input: process.stdin,
      output: process.stdout,
    })
    .question(text, () => {
      process.exit();
    });
}
